package jon1;
import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/test")
public class Servlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
    	String value = request.getParameter("value");

    	int converted = Integer.parseInt(value);

    	
    	
    	response.setContentType("text/html");

    	
    	response.getWriter().println("<!DOCTYPE html>");
    	response.getWriter().println("<html>");
    	response.getWriter().println("  <head>");
    	response.getWriter().println("    <meta charset=\"utf-8\">");
    	response.getWriter().println("    <title>Test page</title>");
    	response.getWriter().println("  </head>");
    	response.getWriter().println("  <body>");
    	response.getWriter().println("    <p> GET" + romanNumerals.translateToNumerals(converted) + "</p>");
    	response.getWriter().println("  </body>");
    	response.getWriter().println("</html>");
    }
    
        @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
    	String value = request.getParameter("value");

    	int converted = Integer.parseInt(value);

    	
    	
    	response.setContentType("text/html");

    	
    	response.getWriter().println("<!DOCTYPE html>");
    	response.getWriter().println("<html>");
    	response.getWriter().println("  <head>");
    	response.getWriter().println("    <meta charset=\"utf-8\">");
    	response.getWriter().println("    <title>Test page</title>");
    	response.getWriter().println("  </head>");
    	response.getWriter().println("  <body>");
    	response.getWriter().println("    <p> POST" + romanNumerals.translateToNumerals(converted) + "</p>");
    	response.getWriter().println("  </body>");
    	response.getWriter().println("</html>");
    }
    
}