package jon1;

enum AlexanderHRomanNumerals {
	  I(1),
//	  IV(4),
	  V(5),
//	  IX(9),
	  X(10),
//	  XL(40),
	  L(50),
//	  XC(90),
	  C(100),
//	  CD(400),
	  D(500),
//	  CM(900),
	  M(1000);
    private int value;
    AlexanderHRomanNumerals(int value) {
        this.value =  value;
    }
    public int getValue() {
    	return value;
    }
    public static int getIndexByChar(char c) {

		for(int j = 0 ; j < AlexanderHRomanNumerals.values().length ; j++) {
			if(AlexanderHRomanNumerals.values()[j].toString().charAt(0) == c) {
				return j;
			}
		}
		return -1;
    }
    
}