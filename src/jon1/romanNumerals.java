package jon1;

import javax.imageio.IIOException;

public class romanNumerals {
	
	
	public static String translateToNumerals(int value) {
	String retStr = "";
	int remainingValue = value;
	
	outer : while(remainingValue > 0) {
		mid : for(int i = AlexanderHRomanNumerals.values().length-1; i > -1 ; --i) {

			int iVal = AlexanderHRomanNumerals.values()[i].getValue();
			if(iVal <= remainingValue) {
				retStr += AlexanderHRomanNumerals.values()[i].toString();
				remainingValue -= iVal;
				continue outer;
			}
			inner : for(int j = i-1; j >= 0 ; j--) {
				int jVal = AlexanderHRomanNumerals.values()[j].getValue();

			 if(remainingValue-(iVal-jVal) >= 0
					&& jVal*10 >=iVal
					&& jVal != iVal-jVal) {
				retStr += AlexanderHRomanNumerals.values()[j].toString()+AlexanderHRomanNumerals.values()[i].toString();
				remainingValue -= iVal-jVal;
				continue outer;
			}
			
			}	
		}
	}
	
	
	return retStr;
	}
	
public static int translateFromNumerals(String numeralsOrg) {
	int retInt = 0;
	String numerals = numeralsOrg.toUpperCase();

	for(int i = 0 ; i < numerals.length() ; i++) {
		char numeral = numerals.charAt(i);
		
		int j = AlexanderHRomanNumerals.getIndexByChar(numeral);
			int value = AlexanderHRomanNumerals.values()[j].getValue();
			
			
			char numeral2 = 'I';
			if(i < numerals.length()-1)
				numeral2 =  numerals.charAt(i+1);
			
			int j2 = AlexanderHRomanNumerals.getIndexByChar(numeral2);
			int value2 = AlexanderHRomanNumerals.values()[j2].getValue();
			

			boolean reductiveNumber = (value < value2);

			
			if(numerals.charAt(i) == numeral) {
		if(!reductiveNumber) {
			retInt += value;
			continue;
		}
		else if(value2 > value && (value*10 >= value2)) {
			retInt += (value2- value );
			i++;
		}
		else {
			try {
				throw new IIOException("bad input");
			} catch (IIOException e) {
				e.printStackTrace();

				System.exit(2);
			}
			
		}
	}
		}
	
	
	return retInt;
	
}
}
